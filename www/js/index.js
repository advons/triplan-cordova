function getUniqueID() {
    function idGenerate() {
        function chr4(){
            return Math.random().toString(16).slice(-4);
        }
        return chr4() + chr4() +
            '-' + chr4() +
            '-' + chr4() +
            '-' + chr4() +
            '-' + chr4() + chr4() + chr4();
    }

    function strIdGenerate() {
        let tmp_id = idGenerate()
        tmp_id = tmp_id.split('-').join('')
        tmp_id = 't' + tmp_id.substring(1, tmp_id.length)
        return tmp_id
    }

    var triplan_unique_id = window.localStorage.getItem("triplan_unique_id")
    if (triplan_unique_id === null) {
        window.localStorage.setItem("triplan_unique_id", triplan_unique_id = strIdGenerate())
        return triplan_unique_id
    } else {
        if (triplan_unique_id.length === 32) {
            return triplan_unique_id
        } else {
            window.localStorage.setItem("triplan_unique_id", triplan_unique_id = strIdGenerate())
            return triplan_unique_id
        }
    }
}

function initWebApplication() {
    $.get("https://triplan-190812.appspot.com/app-settings", function( data ) {
       console.log(data)
       $('iframe#app_frame').attr('src', data.appUrl)
       let poolingTime = (Number(data.gpsPollingInterval) || 5) * 1000
       let gpsStoreUrl = data.gpsStoreUrl
       let userId = getUniqueID()

        var onSuccess = function(position) {
            $.ajax({
                url: gpsStoreUrl,
                type: 'PUT',
                data: {userId: userId, lat: position.coords.latitude, lng: position.coords.longitude},
                success: function(response) {
                    console.log(response)
                }
            });
            console.log('Latitude: '          + position.coords.latitude          + '\n' +
                  'Longitude: '         + position.coords.longitude         + '\n' +
                  'Altitude: '          + position.coords.altitude          + '\n' +
                  'Accuracy: '          + position.coords.accuracy          + '\n' +
                  'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
                  'Heading: '           + position.coords.heading           + '\n' +
                  'Speed: '             + position.coords.speed             + '\n' +
                  'Timestamp: '         + position.timestamp                + '\n');
        }

        function onError(error) {
             console.log('code: '    + error.code    + '\n' +
                  'message: ' + error.message + '\n');
        }

       window.pollingInterval = setInterval(function() {
            navigator.geolocation.getCurrentPosition(onSuccess, onError);
       }, poolingTime)
    });
}

var app = {
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    onDeviceReady: function() {
        initWebApplication()
    },
};

app.initialize();